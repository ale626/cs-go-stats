<?php

class UserController extends BaseController{

	function getUserProfile($steamId){
		$api = new GOApi($steamId);
		$data = $api->getUserData();
		return View::make('user', array('data' => $data));
	}
}