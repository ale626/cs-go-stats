<?php

/**
* Used to make a request to the api and retrieve specific data
*/
class GOApi{

	/**
	 * User steam id. Should be a steam64 id.
	 * @var null
	 */
	private $steamId = null;

	/**
	 * Game Id for Counter Strike Global Offensive.
	 * Id retrieved from https://wiki.teamfortress.com/wiki/WebAPI/GetAppList
	 * Id should not change.
	 * @var string
	 */
	private $gameID = '730';

	/**
	 * Steam api url for stats given game id and steam id.
	 * Documentation at https://wiki.teamfortress.com/wiki/WebAPI/GetUserStatsForGame
	 * @var string
	 */
	private $urlStats = "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v2?";

	/**
	 * Steam api url for user profile given a steam id.
	 * Documentation at https://wiki.teamfortress.com/wiki/WebAPI/GetPlayerSummaries
	 * @var string
	 */
	private $urlPlayer = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002?";

	private $stats = null;

	private $playerInfo = null;

	public function __construct($steamId){
		$this->steamId = $steamId;
	}

	public function getUserData(){
		$gameStatsCurl = curl_init();
		$playerSummaryCurl = curl_init();

		$gameStatsURL = $this->urlStats . "key=" . $_ENV['API_KEY'] .  "&steamid=" . $this->steamId . "&appid={$this->gameID}";
		curl_setopt($gameStatsCurl, CURLOPT_URL, $gameStatsURL);
		curl_setopt($gameStatsCurl, CURLOPT_RETURNTRANSFER , true); // returns result as string

		$playSummaryURL = $this->urlPlayer . "key=" . $_ENV['API_KEY'] .  "&steamids=" . $this->steamId;
		curl_setopt($playerSummaryCurl, CURLOPT_URL, $playSummaryURL);
		curl_setopt($playerSummaryCurl, CURLOPT_RETURNTRANSFER , true); // returns result as string

		// multi curl allows to run multiple curls asynchronously.
		$multiCurl = curl_multi_init();

		// adds curls to a multi curl
		curl_multi_add_handle($multiCurl, $gameStatsCurl);
		curl_multi_add_handle($multiCurl, $playerSummaryCurl);

		$active = null;

		do{
			curl_multi_exec($multiCurl, $active);
		} while($active > 0);

		//get results
		$statsResults = curl_multi_getcontent($gameStatsCurl);
		curl_multi_remove_handle($multiCurl, $gameStatsCurl);

		$userResults = curl_multi_getcontent($playerSummaryCurl);
		curl_multi_remove_handle($multiCurl, $playerSummaryCurl);

		//close multi curl
		curl_multi_close($multiCurl);

		return array('stats' => $statsResults, 'user' => $userResults);
	}

}